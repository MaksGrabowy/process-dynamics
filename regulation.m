clear all
close all
data = readtable('DP_E_220224.csv', 'Delimiter', ';');
s = tf("s");
% t = data.Probki*0.1;

% fs = polyval(f_gain,t);
% gs = polyval(f_time,t);


gain_coe = [1.445 10.82 -22.35 10.7 0.3283];
% gain_coe = [-306.6 908.8 -1008 514 119.5 11.74]

time_coe = [-2161 2561 296 -684.6 128.9];
% time_coe = [5.682e+04 -1703e+05 1.914e+05 -9.909e+04 2.344e+04-1985]

gains_fixed = [1.67788077464789	1.73214595041322	1.58612392561983	1.05669330690537	0.764274182448032	0.753026090909088	0.748293329588020	0.732333178571426];
time_constants_fixed = [18.45 17.4 25.35 121.5 159.9 160 180 180];

h_limits = [0.2 0.36 0.51 0.62 0.70 0.79 0.84 0.93];
set_value = str2double(strrep(data.Wysokosc_odczytana, ',', '.'));
indexes = (set_value ~= 0.8 & set_value ~= 0.0);
t = data.Probki(indexes)*0.1;
h = str2double(strrep(data.Wysokosc_zadana(indexes), ',', '.'));

% figure(1)
% plot(t,h)

h0 = 0.30;
t0 = t(1);

t_n = t(2);
ind = 2;


for j = 1:8
    gain_system = gains_fixed(j);
    tc_system = time_constants_fixed(j);
    if(h0 < h_limits(j))
        break
    end
end

system = gain_system/(1+s*tc_system);

Kp = 0.45*gain_system;
Ti = 0.85*tc_system;
sp = 0.6;
I = 0.0;
h_act = h0;
for t = 1:200
    e = sp - h_act;
    
    P = Kp*e;
    I = I + (1/Ti)*Kp*e*0.1;
    
    u(t) = P+I;
    h_act = P+I+h_act;
end
[Y, Tsim, X] = lsim(system,u,200);
plot(Tsim,Y+h0)
% Kd
% regulator = Kp*(1+(1/(Ki*s)));
% 
% 
% dH = 0.3;
% 
% K_o = system*regulator;
% K_z = K_o/(1+K_o);
% 
% time = 1:1:500;
% u = ones(1,500)*dH;
% 
% [Y, Tsim, X] = lsim(K_z,u,time);
% 
% % pidTuner(system)
