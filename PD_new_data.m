%% Data reading
clear all
close all
% Reading from CSV file generated at laboratory
data = readtable('DP_E_220224.csv', 'Delimiter', ';');

% Choosing correct data columns

% indexes = set_value == 1.6; %change this line to get desirable data series
figure()
figure_count = 1;
t1 = [21.1 19.3 32.1 66.6 93.9 147.3 67.6 72.3];
t2 = [37.4 30.9 45 147.6 220.5 314.7 163.6 150];
gains_tab = [1.67788077464789	1.73214595041322	1.58612392561983	1.05669330690537	0.764274182448032	0.883026090909088	0.608293329588020	0.742333178571426];
gains_tab_fixed = [1.67788077464789	1.73214595041322	1.58612392561983	1.05669330690537	0.764274182448032	0.753026090909088	0.748293329588020	0.732333178571426];

time_constants = [18.4500000000000 17.4000000000000 37.3500000000000 121.500000000000 189.900000000000 251.100000000000 144 116.55]
time_constants_fixed = [18.45 17.4 35.35 121.5 159.9 160 180 180];

for i = 9:16
    set_value = str2double(strrep(data.Wysokosc_odczytana, ',', '.'));
    indexes = set_value == i/10.0;

    readouts = data.Probki(indexes);
    beginning = readouts(1);
    ending = readouts(length(readouts));

    num_of_overlap = int32((ending - beginning)*0.1);
    time = data.Probki(beginning-num_of_overlap:ending+num_of_overlap)*0.1;
    time_readout = data.Probki(beginning:ending)*0.1;

    difference = (length(time) - length(time_readout)/2)*0.1;

    time_normalized = time - time(1);
    measured_height = str2double(strrep(data.Wysokosc_zadana(beginning-num_of_overlap:ending+num_of_overlap), ',', '.'));
    readout_height = str2double(strrep(data.Wysokosc_zadana(beginning:ending), ',', '.'));
    
    for j = 1:length(measured_height)
        corrected_height(j) = measured_height(j) - measured_height(1);
    end

    mean_set = readout_height(end-num_of_overlap+1:end);
    setting_value = mean(mean_set);
    

    subplot(2,4,figure_count)

    plot(time_normalized, corrected_height, color="red");
    hold on
    yline(setting_value - readout_height(1));
    yline((setting_value - readout_height(1))*0.283);
    yline((setting_value - readout_height(1))*0.632);
    setting_values(figure_count) = setting_value;

    % T_16 = 1.5*(t2(figure_count) - t1(figure_count));
    T = time_constants(figure_count);
    T0 = t2(figure_count)-T;

    % Transfer function
    s = tf("s");
    dH = setting_value - readout_height(1);
    dQ = 0.1; % l/min
    k = dH/dQ;

    k_sm = k*60000;

    K_tf = k/(1+T*s);
    N = length(time_normalized);
    V = zeros(N,1) + 0.1;
    t = 0:0.1:time_normalized(length(time_normalized));
    if(figure_count ~= 8)
        waveform = lsim(K_tf, transpose(V),t);
        plot(time_normalized-(time(1) - time_readout(1)),waveform,'color',"blue");
    end
    % interp1(corrected_height-(setting_value - readout_height(1))*0.283,time_normalized,0)
    % time_constants(figure_count) = T;
    % gains_tab(figure_count) = k;
    xlabel('Time');
    ylabel('Measured value');
    text_title = sprintf("%.2f",i/10.0);
    title(text_title);
    grid on;
    figure_count = figure_count +1;

    sample_count = length(indexes);



    clear corrected_height
end


%%
figure
plot(setting_values,gains_tab_fixed)
hold on
xline(0.45)
xline(0.45+0.23)
xline(0.45+0.23+0.45)
xlabel("Height")
ylabel("Gain")

figure()
plot(setting_values,time_constants_fixed)
hold on
xline(0.45)
xline(0.45+0.23)
xline(0.45+0.23+0.45)
xlabel("Height")
ylabel("Time constant")
