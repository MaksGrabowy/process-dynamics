clear all
close all
load ts
load gs
t1_tab = [4.8 9 7.7 59.5 81 200.1 135.6 49.4];
t2_tab = [11.9 37.1 17.9 295.9 222.8 263.8 268.7 93.1];
k_tab = [0.8554 2.1071 1.4552 1.5044 0.4192 0.2559 0.6873 0.5436];
setting_values = [0.1774 0.3935 0.5384 0.6884 0.7301 0.7517 0.8201 0.8769];

figure
plot(setting_values,k_tab)
hold on
yline(0.45)
yline(0.45+0.23)
yline(0.45+0.23+0.45)
ylabel("Height")
xlabel("Gain")
T_tab = zeros(length(t1_tab),0)
T0_tab = zeros(length(t1_tab),0)
for i = 1:length(t1_tab)
    T_tab(i) = 1.5*(t2_tab(i) - t1_tab(i));
    T0_tab(i) = t2_tab(i)-T_tab(i);
end

figure()
plot(setting_values,T_tab)
hold on
yline(0.45)
yline(0.45+0.23)
yline(0.45+0.23+0.45)
ylabel("Height")
xlabel("Time constant")