import csv
import matplotlib.pyplot as plt
import numpy as np
import statistics
import time
from shapely.geometry import LineString

sample_num = []
height = []
set_height = []

height_14 = []
height_15 = []
height_16 = []
height_17 = []
height_18 = []

with open('DP_E.csv', 'r' ,newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    for row in reader:
        if(reader.line_num > 74):
            sample_num.append(int(row[0]))
            height.append(float(row[2]))
            set_height.append(float(row[3]))

            if(True):
                obj = time.strptime(row[1], "%H:%M:%S")
                epoch_time = time.mktime(obj)
                height_14.append((int(row[0]),epoch_time,float(row[2]),float(row[3])))


sample, sample_time ,value, set_val = zip(*height_14)
sample_correction = sample[0]
sample_time_correction = sample_time[0]
sample_corrected = []
sample_time_corrected = []
for item in sample:
    sample_corrected.append(item - sample_correction)

for item in sample_time:
    sample_time_corrected.append(item - sample_time_correction)
mean_height = statistics.mean(value[1100:1300])
print(mean_height)

plt.rcParams["figure.figsize"] = [7.00, 3.50]
plt.rcParams["figure.autolayout"] = True
def mouse_event(event):
   print('x: {} and y: {}'.format(event.xdata, event.ydata))

fig = plt.figure()
cid = fig.canvas.mpl_connect('button_press_event', mouse_event)

plt.plot(sample_time_corrected,value)
# plt.axhline(y = mean_height, color = 'r')
# plt.axhline(y = (mean_height-value[0])*0.283+value[0], color = 'r')
# plt.axhline(y = (mean_height-value[0])*0.632+value[0], color = 'r')
l_283 = np.full(1433,(mean_height-value[0])*0.283)
l_632 = np.array
print(len(value))
# for i in range(len(value)):
#     np.append(l_283,(mean_height-value[0])*0.283)
#     np.append(l_632,(mean_height-value[0])*0.632)

np_value = np.array(value)
np_samples = np.array(sample_corrected)
# idx = np.argwhere(np.diff(np.sign(l_283 - np_value))).flatten()
# plt.plot(l_283[idx], l_283[idx], 'ro')

plt.xlabel('Sample')
plt.ylabel('height')
plt.show()

t1 = 4.996481775051139 
t2 = 11.997932316648125 

# x: 5.001797251553198 and y: 0.11654028381340738
# x: 13.000367256207314 and y: 0.14700680917523257
