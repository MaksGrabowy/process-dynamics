clear all
close all
syms x;
s = tf('s');

gain_coe = [1.445 10.82 -22.35 10.7 0.3283]
% gain_coe = [-306.6 908.8 -1008 514 119.5 11.74]

time_coe = [-2161 2561 296 -684.6 128.9]
% time_coe = [5.682e+04 -1703e+05 1.914e+05 -9.909e+04 2.344e+04-1985]

f_gain = poly2sym(gain_coe)

f_time = poly2sym(time_coe)


data = readtable('DP_E_220224.csv', 'Delimiter', ';');
t = data.Probki*0.1;

% fs = polyval(f_gain,t);
% gs = polyval(f_time,t);
h = str2double(strrep(data.Wysokosc_zadana, ',', '.'));
figure(1)
plot(t,h)
% polyval(f_gain,0.5)
for i = 1:length(t)
    height_sim(i) = polyval(gain_coe,i)*exp(-i/polyval(time_coe,i))/polyval(time_coe,i);
end
figure(2)
plot(t,height_sim)