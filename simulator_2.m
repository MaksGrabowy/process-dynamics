clear all
close all
data = readtable('DP_E_220224.csv', 'Delimiter', ';');
s = tf("s");
% t = data.Probki*0.1;

% fs = polyval(f_gain,t);
% gs = polyval(f_time,t);


gain_coe = [1.445 10.82 -22.35 10.7 0.3283];
% gain_coe = [-306.6 908.8 -1008 514 119.5 11.74]

time_coe = [-2161 2561 296 -684.6 128.9];
% time_coe = [5.682e+04 -1703e+05 1.914e+05 -9.909e+04 2.344e+04-1985]

gains_tab_fixed = [1.67788077464789	1.73214595041322	1.58612392561983	1.05669330690537	0.764274182448032	0.753026090909088	0.748293329588020	0.732333178571426];
time_constants_fixed = [18.45 17.4 25.35 121.5 159.9 160 180 180];

h_limits = [0.2 0.36 0.51 0.62 0.70 0.79 0.84 0.93];
set_value = str2double(strrep(data.Wysokosc_odczytana, ',', '.'));
indexes = (set_value ~= 0.8 & set_value ~= 0.0);
t = data.Probki(indexes)*0.1;
h = str2double(strrep(data.Wysokosc_zadana(indexes), ',', '.'));

% figure(1)
% plot(t,h)

h0 = h(1);
t0 = t(1);

t_n = t(2);
ind = 2;

h_act = h0;
t_act = t0;

Ys = double.empty;
Tsims = double.empty;
for i = 9:16

    set_value = str2double(strrep(data.Wysokosc_odczytana, ',', '.'));
    indexes = set_value == i/10.0;

    readouts = data.Probki(indexes);
    beginning = readouts(1);
    ending = readouts(length(readouts));
    time_readout = data.Probki(beginning:ending)*0.1;
    time_normalized = time_readout - time_readout(1);

    for j = 1:8
        gain_step = gains_tab_fixed(j);
        tc_step = time_constants_fixed(j);
        if(h_act < h_limits(j))
            break
        end
    end


    k_tf = gain_step/(1+s*tc_step);
    u = ones(1,length(time_normalized))*0.1;
    
    [Y, Tsim, X] = lsim(k_tf,u,time_normalized);
    Y_n = Y + h_act;
    Tsim_n = Tsim + t_act;

    h_act = Y_n(length(Y));
    t_act = Tsim_n(length(Tsim));
    
    Ys = cat(1,Ys,Y_n);
    Tsims = cat(1,Tsims,Tsim_n);
end

plot(Tsims,Ys, t, h);
hold on;
yline(0.45)
yline(0.45+0.23)
yline(0.45+0.23+0.45)