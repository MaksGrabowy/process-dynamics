%% Data reading
clear all
close all
% Reading from CSV file generated at laboratory
data = readtable('DP_sekcja_data.csv', 'Delimiter', ';');

% Choosing correct data columns
set_value = str2double(strrep(data.Wysokosc_odczytana, ',', '.'));
indexes = set_value == 1.5; %change this line to get desirable data series
time = data.Probki(indexes)*0.1;
time_normalized = time - time(1);
measured_height = str2double(strrep(data.Wysokosc_zadana(indexes), ',', '.'));

for i = 1:length(measured_height)
    corrected_height(i) = measured_height(i) - measured_height(1);
end

figure;
plot(time_normalized, corrected_height, color="red");
hold on
xlabel('Time');
ylabel('Measured value');
title('');
grid on; 

%% now we have to make measurments by "hand"
% start with calculating setting value from last 500 samples

mean_set = measured_height(end-500+1:end);
setting_value = sum(mean_set)/500;
yline(setting_value - measured_height(1));
yline((setting_value - measured_height(1))*0.283);
yline((setting_value - measured_height(1))*0.632);

%now we just check on plot times at which measurment crosses horizontal
%lines

% t1 = 46.9;
% t2 = 93.1;

t1 = 4.8 %1.5
t2 = 11.9

t1 = 9 %1.6
t2 = 37.1
% 
% t1 = 7.7 %1.7
% t2 = 17.9

t1 = 59.5 %1.8
t2 = 295.9

% t1 = 81 %1.9
% t2 = 222.8
% 
t1 = 200.1 %2.0
t2 = 263.8
% 
% t1 = 135.6 %2.1
% t2 = 268.7
% 
% t1 = 49.4 %2.2
% t2 = 93.1

T = 1.5*(t2 - t1);
T0 = t2-T;

%% Transfer function
s = tf("s")
dH = setting_value - measured_height(1)
dQ = 0.1; % l/min
k = dH/dQ;T = 1.5*(t2 - t1);
T0 = t2-T;
k_sm = k*60000

K_tf = k/(1+T*s);
N = length(time);
V = zeros(N,1) + 0.1;
t = 0:0.1:time_normalized(length(time_normalized));

lsim(K_tf, transpose(V),t);

% Kr(h) , Ti(h)