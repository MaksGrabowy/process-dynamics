clear all
close all
data = readtable('DP_E_220224.csv', 'Delimiter', ';');
s = tf("s");
% t = data.Probki*0.1;

% fs = polyval(f_gain,t);
% gs = polyval(f_time,t);


gain_coe = [1.445 10.82 -22.35 10.7 0.3283];
% gain_coe = [-306.6 908.8 -1008 514 119.5 11.74]

time_coe = [-2161 2561 296 -684.6 128.9];
% time_coe = [5.682e+04 -1703e+05 1.914e+05 -9.909e+04 2.344e+04-1985]

gains_tab_fixed = [1.67788077464789*1.2	1.73214595041322*1.07	1.58612392561983*1.11	1.45669330690537*1.1	1.444274182448032	1.313026090909088	1.248293329588020	1.132333178571426];
time_constants_fixed = [18.45 17.4 25.35 121.5 159.9 160 180 180];

h_limits = [0.2 0.375 0.529 0.636 0.716 0.79 0.84 0.93];
set_value = str2double(strrep(data.Wysokosc_odczytana, ',', '.'));
indexes = (set_value ~= 0.8 & set_value ~= 0.0);
t = data.Probki(indexes)*0.1;

t_normalized = t - t(1);
u = set_value(indexes)-0.8;
h = str2double(strrep(data.Wysokosc_zadana(indexes), ',', '.'));

% figure(1)
% plot(t,h)
h_act = h(1);
dh = 0.1;
for i = 1:length(t_normalized)
    hs(i) = h_act;

    for j = 1:8
        gain_act = gains_tab_fixed(j);
        tc_act = time_constants_fixed(j);
        if(h_act <= h_limits(j))
            break
        end
    end

    % if(h_act < 0.2)
    %     h_act = 0.2;
    % end
    % gain_act = polyval(gain_coe,h_act);
    % tc_act = polyval(time_coe,h_act);

    h_act = (u(i)*gain_act/tc_act - h_act*1/tc_act)*dh + h_act;
end

plot(t_normalized,hs, t_normalized, h);